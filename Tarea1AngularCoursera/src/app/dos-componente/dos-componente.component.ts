import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dos-componente',
  templateUrl: './dos-componente.component.html',
  styleUrls: ['./dos-componente.component.css']
})
export class DosComponenteComponent implements OnInit {

    vector: String[];
  
  
  constructor() {
    this.vector = ['uno','dos','tres'];
  
  }

  ngOnInit(): void {
  }

}
