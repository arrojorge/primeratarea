import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DosComponenteComponent } from './dos-componente.component';

describe('DosComponenteComponent', () => {
  let component: DosComponenteComponent;
  let fixture: ComponentFixture<DosComponenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DosComponenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DosComponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
