import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnComponenteComponent } from './un-componente.component';

describe('UnComponenteComponent', () => {
  let component: UnComponenteComponent;
  let fixture: ComponentFixture<UnComponenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnComponenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnComponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
