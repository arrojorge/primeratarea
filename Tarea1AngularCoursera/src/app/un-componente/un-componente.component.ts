import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-un-componente',
  templateUrl: './un-componente.component.html',
  styleUrls: ['./un-componente.component.css']
})
export class UnComponenteComponent implements OnInit {

   
    @Input() text: string;
    vector: string[]=new Array();
    i: number = 0;
    @HostBinding('attr.class') cssClass='cambio';
  
  constructor() {
    
    
  }

  ngOnInit(): void {
  }
  
  guardarVector(t: string){
    this.vector[this.i]=t
    this.i=this.i+1;
  }
  
  pintar(t: string):boolean{
    console.log(t);
    console.log(this.i);
    this.text = t;
    this.guardarVector(t);
    return false;
  }
  
 
  
 

}
