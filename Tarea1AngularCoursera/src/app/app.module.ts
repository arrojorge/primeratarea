import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UnComponenteComponent } from './un-componente/un-componente.component';
import { DosComponenteComponent } from './dos-componente/dos-componente.component';

@NgModule({
  declarations: [
    AppComponent,
    UnComponenteComponent,
    DosComponenteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
